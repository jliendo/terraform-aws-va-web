resource "aws_ecs_service" "ecs_service_api" {
  name = "ECS_Service_for_API"
  cluster = aws_ecs_cluster.ecs_cluster.arn

  deployment_circuit_breaker {
    enable   = "false"
    rollback = "false"
  }

  deployment_controller {
    type = "ECS"
  }

  deployment_maximum_percent = "200"
  deployment_minimum_healthy_percent = "100"
  desired_count = "2"
  enable_ecs_managed_tags = "false"
  enable_execute_command = "false"
  health_check_grace_period_seconds = "45"
  launch_type = "FARGATE"

  load_balancer {
    container_name   = "pruebas-replica-api"
    container_port   = "80"
    target_group_arn = "${data.terraform_remote_state.nlb.outputs.lbtg_1_arn}"
  }


  network_configuration {
    assign_public_ip = "false"
    security_groups  = [aws_security_group.ecs_service_sg.id]
    subnets          = [
      "${data.terraform_remote_state.networking.outputs.sn_priv_1_id}",
      "${data.terraform_remote_state.networking.outputs.sn_priv_2_id}",
      "${data.terraform_remote_state.networking.outputs.sn_priv_3_id}"
    ]
  }

  task_definition = aws_ecs_task_definition.task_api.arn

  platform_version    = "LATEST"
  scheduling_strategy = "REPLICA"

  tags = {
    deploy = "terraform"
  }

}
