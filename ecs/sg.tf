resource "aws_security_group" "ecs_service_sg" {
  name = "allow_80"
  description = "Permite HTTP del NLB"
  vpc_id = "${data.terraform_remote_state.networking.outputs.vpc_main_id}"

  ingress {
    description = "allow tcp 80 in"
    from_port = 0
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "allow all out"
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Permite HTTP a/del NLB"
    deploy = "terraform"
  }
}
