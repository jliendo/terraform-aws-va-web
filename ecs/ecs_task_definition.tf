# para los logs del task en cloudwatch
resource "aws_cloudwatch_log_group" "log_prueba_replica_api" {
  name = "ecs_prueba_repica_api"

  tags = {
    deploy = " terraform"
  }
}

resource "aws_ecs_task_definition" "task_api" {
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = "256"
  memory                   = "512"
  task_role_arn            = aws_iam_role.ecs_task_role.arn
  execution_role_arn       = aws_iam_role.ecs_task_execution_role.arn
  family                   = "prueba-replica-api"
  container_definitions    = <<EOF
[
  {
    "cpu":0,
    "name":"pruebas-replica-api",
    "environment":[],
    "essential":true,
    "image":"311896453677.dkr.ecr.us-east-1.amazonaws.com/apitest:latest",
    "logConfiguration": {
      "logDriver":"awslogs",
      "options":{
        "awslogs-group":"${aws_cloudwatch_log_group.log_prueba_replica_api.name}",
        "awslogs-region":"us-east-1",
        "awslogs-stream-prefix":"ecs"
      }
    },
    "mountPoints":[],
    "portMappings":[
      {
        "containerPort":80,
        "hostPort":80,
        "protocol":"tcp"
      }
    ],
    "ulimits":[
      {
        "hardLimit":65535,
        "name":"nofile",
        "softLimit":65535
      }
    ],
    "volumesFrom":[]
  }
]
EOF

  tags = {
    deploy = "terraform"
  }
}
