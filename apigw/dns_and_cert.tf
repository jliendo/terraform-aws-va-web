data "aws_route53_zone" "aws_smt_zone" {
  name = "aws.smartmonkeytech.com"
}

# crear el cert para el api_gateway
resource "aws_acm_certificate" "api_cert" {
  domain_name = "api.aws.smartmonkeytech.com"
  validation_method = "DNS"

  tags = {
    deploy = "terraform"
  }
}

# en aws.smartmonkeytech.com crea un registro de validacion
# por cada una de las opciones de validacion
resource "aws_route53_record" "aws_smt_val_recs" {
  for_each = {
    for dvo in aws_acm_certificate.api_cert.domain_validation_options:
      dvo.domain_name => {
        name = dvo.resource_record_name
        record = dvo.resource_record_value
        type = dvo.resource_record_type
      }
  }

  allow_overwrite = true
  name = each.value.name
  records = [each.value.record]
  ttl = 60
  type = each.value.type
  zone_id = data.aws_route53_zone.aws_smt_zone.zone_id

  depends_on = [
    aws_acm_certificate.api_cert
  ]
}

resource "aws_acm_certificate_validation" "aws_smt_val" {
  certificate_arn = aws_acm_certificate.api_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.aws_smt_val_recs : record.fqdn]

  depends_on = [
    aws_route53_record.aws_smt_val_recs
  ]
}

resource "aws_api_gateway_domain_name" "api_domain_name" {
  domain_name = aws_acm_certificate.api_cert.domain_name
  regional_certificate_arn = aws_acm_certificate_validation.aws_smt_val.certificate_arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  depends_on = [
    aws_acm_certificate_validation.aws_smt_val
  ]

  tags = {
    deploy = "terraform"
  }
}

resource "aws_route53_record" "api_smt" {
  allow_overwrite = true
  name = "api"
  type = "A"
  zone_id = data.aws_route53_zone.aws_smt_zone.id

  alias {
    name = aws_api_gateway_domain_name.api_domain_name.regional_domain_name
    zone_id = aws_api_gateway_domain_name.api_domain_name.regional_zone_id
    evaluate_target_health = false
  }

  depends_on = [
    aws_api_gateway_domain_name.api_domain_name
  ]
}

resource "aws_api_gateway_base_path_mapping" "api_dns_map" {
  api_id = aws_api_gateway_rest_api.apigw_main.id
  stage_name = aws_api_gateway_stage.awpigw_stage.stage_name
  domain_name = aws_api_gateway_domain_name.api_domain_name.domain_name

  depends_on = [
    aws_route53_record.api_smt
  ]
}
