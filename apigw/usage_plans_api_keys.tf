resource "aws_api_gateway_usage_plan" "api_usage_plan" {
  name = "FullAccess"
  description = "Usage plan Full Access"

  api_stages {
    api_id = aws_api_gateway_rest_api.apigw_main.id
    stage = aws_api_gateway_stage.awpigw_stage.stage_name
  }

  tags = {
    deploy = "terraform"
  }

}

resource "aws_api_gateway_api_key" "key1" {
  name = "ApiKey-FullAccess-PWA-Prod"
  value = "zasqyJdSc92MhWMxYu6vW3hqhxLuDwKog3mqoYkf"

  tags = {
    deploy = "terraform"
  }
}

resource "aws_api_gateway_usage_plan_key" "usage_key1" {
  key_id = aws_api_gateway_api_key.key1.id
  key_type = "API_KEY"
  usage_plan_id = aws_api_gateway_usage_plan.api_usage_plan.id
}

