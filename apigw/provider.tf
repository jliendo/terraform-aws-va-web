provider "aws" {
  region = "us-east-1"
  profile = "aws-test-2"
}

terraform {
	required_providers {
		aws = {
	    version = "~> 3.48.0"
		}
  }
}
