resource "aws_api_gateway_vpc_link" "vpc_link" {
  name = "apigw-nlb-vpclink"
  description = "VPCLink to fwd reqs to NLB"
  target_arns = [
    "${data.terraform_remote_state.nlb.outputs.nlb_main_arn}"
  ]

  tags = {
    deploy = "terraform"
  }

}

resource "aws_api_gateway_rest_api" "apigw_main" {
  name = "generic-api"
  description = "VA REST generic-api"
  api_key_source = "HEADER"
  body = templatefile(
    "va_apigw.json",
    {
      stageVariables = {
        VPCLink = aws_api_gateway_vpc_link.vpc_link.id
        # por el momento
        genericApiHostname = "nlb.aws.smartmonkeytech.com"
      }
    }
  )

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  depends_on = [
    aws_api_gateway_vpc_link.vpc_link
  ]

  tags = {
    deploy = "terraform"
  }
}


resource "aws_api_gateway_deployment" "apigw_deployment" {
  rest_api_id = aws_api_gateway_rest_api.apigw_main.id

  triggers = {
    redeployment = sha1(jsonencode(aws_api_gateway_rest_api.apigw_main.body))
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "awpigw_stage" {
  deployment_id = aws_api_gateway_deployment.apigw_deployment.id
  rest_api_id = aws_api_gateway_rest_api.apigw_main.id
  stage_name = "Prod"
  variables = {
    allowHeaders = "Content-Type,X-Channel,x-channel,x-api-key,x-clientversion,Authorization"
    VPCLink = aws_api_gateway_vpc_link.vpc_link.id
    allowOrigins = "https://vivaaerobus.com,https://www.vivaaerobus.com,https://staging.vivaaerobus.com,https://pwa.int.vivaaerobus.io,https://pwa-staging.int.vivaaerobus.io,https://beta.vivaaerobus.com,https://staging.web.viva.jaque.dev"
    genericApiHostname = "api-proxy.int.vivaaerobus.com"
  }

  tags = {
    deploy = "terraform"
  }
}
