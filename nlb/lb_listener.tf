resource "aws_lb_listener" "listener_443" {
  certificate_arn = aws_acm_certificate.nlb_cert.arn

  default_action {
    order            = "1"
    target_group_arn = aws_lb_target_group.lbtg_1.arn
    type             = "forward"
  }

  load_balancer_arn = aws_lb.nlb_main.arn
  port              = "443"
  protocol          = "TLS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"

  depends_on = [
    aws_acm_certificate_validation.aws_smt_val
  ]

  tags = {
    deploy = "terraform"
  }
}
