output "nlb_main_arn" {
  value = aws_lb.nlb_main.arn
}

output "lbtg_1_arn" {
  value = aws_lb_target_group.lbtg_1.arn
}
