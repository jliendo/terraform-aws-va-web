# crear el cert para el network load balancer
# el registro nlb debe estar creado en route53
resource "aws_acm_certificate" "nlb_cert" {
  domain_name = "nlb.aws.smartmonkeytech.com"
  validation_method = "DNS"

  tags = {
    deploy = "terraform"
  }

  depends_on = [
    aws_route53_record.nlb_smt
  ]
}

# en aws.smartmonkeytech.com crea un registro de validacion
# por cada una de las opciones de validacion
resource "aws_route53_record" "aws_smt_val_recs" {
  for_each = {
    for dvo in aws_acm_certificate.nlb_cert.domain_validation_options:
      dvo.domain_name => {
        name = dvo.resource_record_name
        record = dvo.resource_record_value
        type = dvo.resource_record_type
      }
  }

  allow_overwrite = true
  name = each.value.name
  records = [each.value.record]
  ttl = 60
  type = each.value.type
  zone_id = data.aws_route53_zone.aws_smt_zone.zone_id
}

resource "aws_acm_certificate_validation" "aws_smt_val" {
  certificate_arn = aws_acm_certificate.nlb_cert.arn
  validation_record_fqdns = [for record in aws_route53_record.aws_smt_val_recs : record.fqdn]
}
