resource "aws_lb_target_group" "lbtg_1" {
  name = "lbtg-1"
  protocol = "TCP"
  port = "80"
  target_type = "ip"
  preserve_client_ip = "false"
  proxy_protocol_v2 = "false"
  deregistration_delay = "30"
  vpc_id = "${data.terraform_remote_state.networking.outputs.vpc_main_id}"

  health_check {
    enabled = "true"
    healthy_threshold = "3"
    interval = "30"
    port = "80"
    protocol = "TCP"
    # timeout = "10"
    unhealthy_threshold = "3"
  }

  stickiness {
    cookie_duration = "0"
    enabled = "false"
    type = "source_ip"
  }

  tags = {
    Name = "lbtg_1"
    deploy = "terraform"
  }
}
