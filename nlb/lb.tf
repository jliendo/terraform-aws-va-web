resource "aws_lb" "nlb_main" {
  name = "nlb-main"
  load_balancer_type = "network"
  ip_address_type = "ipv4"
  internal = "true"
  enable_cross_zone_load_balancing = "false"
  enable_deletion_protection = "false"

  tags = {
    Name = "nlb_main"
    deploy = "terraform"
  }

  subnets = [
    "${data.terraform_remote_state.networking.outputs.sn_priv_1_id}",
    "${data.terraform_remote_state.networking.outputs.sn_priv_2_id}",
    "${data.terraform_remote_state.networking.outputs.sn_priv_3_id}"
  ]
}
