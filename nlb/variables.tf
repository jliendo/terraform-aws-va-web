data "terraform_remote_state" "networking" {
  backend = "local"

  config = {
    path = "../../va-self-compose/networking/terraform.tfstate"
  }
}
