data "aws_route53_zone" "aws_smt_zone" {
  name = "aws.smartmonkeytech.com"
}

resource "aws_route53_record" "nlb_smt" {
  allow_overwrite = true
  name = "nlb"
  type = "A"
  zone_id = data.aws_route53_zone.aws_smt_zone.id

  alias {
    name = aws_lb.nlb_main.dns_name
    zone_id = aws_lb.nlb_main.zone_id
    evaluate_target_health = false
  }

  depends_on = [
    aws_lb.nlb_main
  ]
}
