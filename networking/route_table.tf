# las rutas para las subnets privadas todas apuntan
# por default al natgw de su subred
resource "aws_route_table" "rt_sn_priv_1" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw_1.id
  }
  tags = {
    Name = "rt_sn_priv_1"
    deploy = "terraform"
  }
}

resource "aws_route_table" "rt_sn_priv_2" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw_2.id
  }
  tags = {
    Name = "rt_sn_priv_2"
    deploy = "terraform"
  }
}

resource "aws_route_table" "rt_sn_priv_3" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.natgw_3.id
  }
  tags = {
    Name = "rt_sn_priv_3"
    deploy = "terraform"
  }
}

# las rutas para las subnets publicas todas apuntan
# por default al igw de la vpc
resource "aws_route_table" "rt_sn_pub_1" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_main.id
  }
  tags = {
    Name = "rt_sn_pub_1"
    deploy = "terraform"
  }
}

resource "aws_route_table" "rt_sn_pub_2" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_main.id
  }
  tags = {
    Name = "rt_sn_pub_2"
    deploy = "terraform"
  }
}

resource "aws_route_table" "rt_sn_pub_3" {
  vpc_id = aws_vpc.vpc_main.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw_main.id
  }
  tags = {
    Name = "rt_sn_pub_3"
    deploy = "terraform"
  }
}

# las rutas para las subnets privadas todas apuntan
# por default al natgw de su subred
resource "aws_route_table_association" "rt_assoc_1" {
  subnet_id = aws_subnet.sn_priv_1.id
  route_table_id = aws_route_table.rt_sn_priv_1.id
}

resource "aws_route_table_association" "rt_assoc_2" {
  subnet_id = aws_subnet.sn_priv_2.id
  route_table_id = aws_route_table.rt_sn_priv_2.id
}

resource "aws_route_table_association" "rt_assoc_3" {
  subnet_id = aws_subnet.sn_priv_3.id
  route_table_id = aws_route_table.rt_sn_priv_3.id
}

# las rutas para las subnets publicas todas apuntan
# por default al igw de la vpc
resource "aws_route_table_association" "rt_assoc_4" {
  subnet_id = aws_subnet.sn_pub_1.id
  route_table_id = aws_route_table.rt_sn_pub_1.id
}

resource "aws_route_table_association" "rt_assoc_5" {
  subnet_id = aws_subnet.sn_pub_2.id
  route_table_id = aws_route_table.rt_sn_pub_2.id
}

resource "aws_route_table_association" "rt_assoc_6" {
  subnet_id = aws_subnet.sn_pub_3.id
  route_table_id = aws_route_table.rt_sn_pub_3.id
}
