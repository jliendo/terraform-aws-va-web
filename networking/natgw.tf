resource "aws_nat_gateway" "natgw_1" {
  subnet_id = aws_subnet.sn_pub_1.id
  allocation_id = aws_eip.nat_eip_1.id

  tags = {
    Name = "natgw_1"
    deploy = "terraform"
    pub_ip = aws_eip.nat_eip_1.public_ip
  }
}

resource "aws_nat_gateway" "natgw_2" {
  subnet_id = aws_subnet.sn_pub_2.id
  allocation_id = aws_eip.nat_eip_2.id

  tags = {
    Name = "natgw_2"
    deploy = "terraform"
    pub_ip = aws_eip.nat_eip_2.public_ip
  }
}

resource "aws_nat_gateway" "natgw_3" {
  subnet_id = aws_subnet.sn_pub_3.id
  allocation_id = aws_eip.nat_eip_3.id

  tags = {
    Name = "natgw_3"
    deploy = "terraform"
    pub_ip = aws_eip.nat_eip_3.public_ip
  }
}
