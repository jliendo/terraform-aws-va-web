resource "aws_eip" "nat_eip_1" {
  network_border_group = "us-east-1"
  public_ipv4_pool = "amazon"
  vpc = true

  tags = {
    deploy = "terraform"
  }
}

resource "aws_eip" "nat_eip_2" {
  network_border_group = "us-east-1"
  public_ipv4_pool = "amazon"
  vpc = true
  tags = {
    deploy = "terraform"
  }
}

resource "aws_eip" "nat_eip_3" {
  network_border_group = "us-east-1"
  public_ipv4_pool = "amazon"
  vpc = true
  tags = {
    deploy = "terraform"
  }
}
