resource "aws_subnet" "sn_priv_1" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.13.0/26"
  availability_zone = "us-east-1a"
  tags = {
    Name = "sn_priv_1"
    deploy = "terraform"
  }
}

resource "aws_subnet" "sn_priv_2" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.13.64/26"
  availability_zone = "us-east-1b"
  tags = {
    Name = "sn_priv_2"
    deploy = "terraform"
  }
}

resource "aws_subnet" "sn_priv_3" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.13.192/26"
  availability_zone = "us-east-1c"
  tags = {
    Name = "sn_priv_3"
    deploy = "terraform"
  }
}

resource "aws_subnet" "sn_pub_1" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.12.0/26"
  availability_zone = "us-east-1a"
  tags = {
    Name = "sn_pub_1"
    deploy = "terraform"
  }
}

resource "aws_subnet" "sn_pub_2" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.12.128/26"
  availability_zone = "us-east-1b"
  tags = {
    Name = "sn_pub_2"
    deploy = "terraform"
  }
}
resource "aws_subnet" "sn_pub_3" {
  vpc_id = aws_vpc.vpc_main.id
  cidr_block = "172.17.14.0/26"
  availability_zone = "us-east-1c"
  tags = {
    Name = "sn_pub_3"
    deploy = "terraform"
  }
}
