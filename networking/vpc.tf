resource "aws_vpc" "vpc_main" {
  cidr_block = "172.17.12.0/22"

  tags = {
    Name = "main"
    deploy = "terraform"
  }
}
