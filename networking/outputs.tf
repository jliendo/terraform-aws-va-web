output "vpc_main_id" {
  value = aws_vpc.vpc_main.id
}

output "sn_priv_1_id" {
  value = aws_subnet.sn_priv_1.id
}

output "sn_priv_2_id" {
  value = aws_subnet.sn_priv_2.id
}

output "sn_priv_3_id" {
  value = aws_subnet.sn_priv_3.id
}

output "sn_pub_1_id" {
  value = aws_subnet.sn_pub_1.id
}

output "sn_pub_2_id" {
  value = aws_subnet.sn_pub_2.id
}

output "sn_pub_3_id" {
  value = aws_subnet.sn_pub_3.id
}
